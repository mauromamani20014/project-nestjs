import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { setupApp } from '../src/setup-app';

describe('Authentication system (e2e)', () => {
  let app: INestApplication;
  const userData = {
    user: { email: `test@test.com`, password: '123' },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    setupApp(app);
    await app.init();
  });

  it('Should handles a signup request', () => {
    return request(app.getHttpServer())
      .post('/auth/signup')
      .send(userData)
      .expect(201)
      .then((res) => {
        const { id, email } = res.body;
        expect(id).toBeDefined();
        expect(email).toBe(userData.user.email);
      });
  });

  it('Should login as a new user and then get the currently user', async () => {
    const res = await request(app.getHttpServer())
      .post('/auth/signup')
      .send(userData)
      .expect(201);

    const cookie = res.get('Set-Cookie');

    const { body } = await request(app.getHttpServer())
      .get('/auth/whoami')
      .set('Cookie', cookie)
      .expect(200);

    expect(body.email).toBe(userData.user.email);
  });
});
