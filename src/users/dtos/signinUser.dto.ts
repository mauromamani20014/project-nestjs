import { IsEmail, IsString } from 'class-validator';

export class SignInUserDto {
  @IsEmail()
  readonly email: string;

  @IsString()
  readonly password: string;
}
