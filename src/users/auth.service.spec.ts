import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { CreateUserDto } from './dtos/createUser.dto';
import { UserEntity } from './user.entity';
import { UsersService } from './users.service';

describe('AuthService', () => {
  let service: AuthService;
  let mockUsersService: Partial<UsersService>;
  const userData: CreateUserDto = {
    email: 'test@test.com',
    password: 'tester',
  };

  beforeEach(async () => {
    // Create a fake copy of the users service
    const users: UserEntity[] = [];
    mockUsersService = {
      find: (email: string) =>
        Promise.resolve(users.filter((user) => user.email === email)),
      create: (createUserDto: CreateUserDto) => {
        const user = {
          id: Math.random(),
          email: createUserDto.email,
          password: createUserDto.password,
        } as UserEntity;
        users.push(user);
        return Promise.resolve(user);
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: UsersService, useValue: mockUsersService },
      ],
    }).compile();

    service = module.get(AuthService);
  });

  it('Should create an instance of auth service', () => {
    expect(service).toBeDefined();
  });

  it('Should create a new user with hashed password', async () => {
    const user = await service.signup(userData);

    expect(user.password).not.toBe(userData.password);
    expect(user.email).toBe(userData.email);
    expect(user.id).toBeDefined();
  });

  it('Should throw an error if user signs up with an existing email', async () => {
    await service.signup(userData);
    try {
      await service.signup(userData);
    } catch (error) {
      expect(error).toEqual(
        new HttpException('Email in use!', HttpStatus.BAD_REQUEST),
      );
    }
  });

  it('Should throws if signin is called with an unused email', async () => {
    try {
      await service.signin(userData);
    } catch (error) {
      expect(error).toEqual(
        new HttpException('User not found!', HttpStatus.NOT_FOUND),
      );
    }
  });

  it('Should throws if an invalid password is provided', async () => {
    await service.signup({ email: 't@t', password: 'password' });
    try {
      await service.signin({ email: 't@t', password: 'password!!' });
    } catch (error) {
      expect(error).toEqual(
        new HttpException('Bad password!', HttpStatus.BAD_REQUEST),
      );
    }
  });

  it('Should return an user if correct password is provided', async () => {
    await service.signup(userData);
    const user = await service.signin(userData);
    expect(user).toBeDefined();
  });
});
