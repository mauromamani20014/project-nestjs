import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { CreateUserDto } from './dtos/createUser.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  public create(createUserDto: CreateUserDto): Promise<UserEntity> {
    const user = this.userRepository.create(createUserDto);
    return this.userRepository.save(user);
  }

  public async findOne(id: number): Promise<UserEntity> {
    if (!id) return null;

    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
    }

    return user;
  }

  public find(email: string): Promise<UserEntity[]> {
    return this.userRepository.find({ email });
  }

  public findAll(): Promise<UserEntity[]> {
    return this.userRepository.find();
  }

  public async update(
    id: number,
    attrs: Partial<UserEntity>,
  ): Promise<UserEntity> {
    const user = await this.findOne(id);

    Object.assign(user, attrs);
    // Usamos el save() para que los hooks de typeorm se disparen, caso contrario usariamos el update()
    return this.userRepository.save(user);
  }

  public async remove(id: number): Promise<UserEntity> {
    const user = await this.findOne(id);

    return this.userRepository.remove(user);
  }
}
