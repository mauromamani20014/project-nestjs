import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UserEntity } from './user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let controller: UsersController;
  let mockUsersService: Partial<UsersService>;
  let mockAuthService: Partial<AuthService>;
  const mockUserData = {
    id: 1,
    email: 'test@test.com',
    password: 'test',
  };

  beforeEach(async () => {
    mockAuthService = {
      //signup: () => {},
      signin: ({ email, password }) =>
        Promise.resolve({ id: 1, email, password } as UserEntity),
    };

    mockUsersService = {
      findOne: (id: number) =>
        Promise.resolve({ ...mockUserData, id } as UserEntity),
      find: (email: string) =>
        Promise.resolve([{ ...mockUserData, email }] as UserEntity[]),
      remove: (id: number) =>
        Promise.resolve({ ...mockUserData, id } as UserEntity),
      //update: () => {},
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { provide: UsersService, useValue: mockUsersService },
        { provide: AuthService, useValue: mockAuthService },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAllUsers method should return a list of users', async () => {
    const users = await controller.findAllUsers('test@test.com');
    expect(users.length).toBe(1);
    expect(users[0].email).toBe('test@test.com');
  });

  it('findOne method should return an user with the given id', async () => {
    const user = await controller.findUser(1);
    expect(user).toBeDefined();
  });

  it('signin updates session object and return an user', async () => {
    const session = { uid: null };
    const user = await controller.signIn(
      { email: 't@t', password: '123' },
      session,
    );

    expect(user.id).toBe(1);
    expect(session.uid).toBe(1);
  });
});
