import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
//import { RequestWithUser } from '../types/requestWithUser.interface';
import { UserEntity } from '../user.entity';
import { UsersService } from '../users.service';

declare global {
  namespace Express {
    interface Request {
      user?: UserEntity;
    }
  }
}

@Injectable()
export class CurrentUserMiddleware implements NestMiddleware {
  constructor(private readonly usersService: UsersService) {}

  async use(req: Request, _res: Response, next: NextFunction) {
    const { uid } = req.session || {};

    if (uid) {
      const user = await this.usersService.findOne(uid);
      req.user = user;
    }

    next();
  }
}
