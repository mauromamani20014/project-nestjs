import {
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Injectable,
} from '@nestjs/common';
import { UsersService } from '../users.service';

/*
 * REQUEST -> MIDDLEWARE -> GUARD -> INTERCEPTOR -> HANDLE REQUEST -> INTERCEPTOR -> RESPONSE
 * Vamos a transformar este interceptor a un middleware, para que el guard funcione
 * */
@Injectable()
export class UserInterceptor implements NestInterceptor {
  constructor(private readonly usersService: UsersService) {}

  async intercept(context: ExecutionContext, next: CallHandler) {
    const request = context.switchToHttp().getRequest();
    const { uid } = request.session || {};

    if (uid) {
      const user = await this.usersService.findOne(uid);
      request.user = user;
    }

    return next.handle();
  }
}
