import { createParamDecorator, ExecutionContext } from '@nestjs/common';

type UserData = 'id' | 'email';

/* Returns the current User */
export const User = createParamDecorator(
  (data: UserData, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    if (!request.user) {
      return null;
    }

    if (data) {
      return request.user[data];
    }

    return request.user;
  },
);
