import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Session,
  UseGuards,
  //UsePipes,
  //ValidationPipe,
  // UseInterceptors,
  //ClassSerializerInterceptor,
} from '@nestjs/common';
import { Serialize } from '../interceptors/serialize.interceptor';
import { CreateUserDto } from './dtos/createUser.dto';
import { UpdateUserDto } from './dtos/updateUser.dto';
import { UserDto } from './dtos/user.dto';
import { UserEntity } from './user.entity';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { SignInUserDto } from './dtos/signinUser.dto';
//import { UserInterceptor } from './interceptors/user.interceptor';
import { User } from './decorators/user.decorator';
import { AuthGuard } from '../guards/auth.guard';

@Controller('/auth')
@Serialize(UserDto)
//@UseInterceptors(UserInterceptor)
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @Get('/whoami')
  @UseGuards(AuthGuard)
  public whoAmI(@User() user: UserEntity) {
    return user;
  }

  @Post('/signout')
  public async signOut(@Session() session: any): Promise<any> {
    session.uid = null;
  }

  @Post('/signup')
  public async createUser(
    @Body('user') createUserDto: CreateUserDto,
    @Session() session: any,
  ): Promise<UserEntity> {
    const user = await this.authService.signup(createUserDto);
    session.uid = user.id;

    return user;
  }

  @Post('/signin')
  //@UsePipes(new ValidationPipe({ whitelist: true }))
  public async signIn(
    @Body('user') signinUserDto: SignInUserDto,
    @Session() session: any,
  ): Promise<UserEntity> {
    const user = await this.authService.signin(signinUserDto);
    session.uid = user.id;

    return user;
  }

  @Get()
  public findUsersByEmail(
    @Query('email') email: string,
  ): Promise<UserEntity[]> {
    return this.usersService.find(email);
  }

  @Get('/all')
  public findAllUsers(): Promise<UserEntity[]> {
    return this.usersService.findAll();
  }

  // Interceptamos la clase instanciada y usamos algunas reglas definidas en el modelo, @Exclude, @Include etc
  //  @UseInterceptors(new SerializeInterceptor(UserDto))
  // @Serialize(UserDto)
  @Get('/:id')
  public findUser(@Param('id') id: number): Promise<UserEntity> {
    return this.usersService.findOne(id);
  }

  @Patch('/:id')
  //@UsePipes(new ValidationPipe({ whitelist: true }))
  public updateUser(
    @Param('id') id: number,
    @Body('user') updateUserDto: UpdateUserDto,
  ): Promise<UserEntity> {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete('/:id')
  public removeUser(@Param('id') id: number): Promise<UserEntity> {
    return this.usersService.remove(id);
  }
}
