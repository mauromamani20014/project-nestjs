import { hashPassword, comparePassword } from '../utils/bcrypt.util';
import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { CreateUserDto } from './dtos/createUser.dto';
import { SignInUserDto } from './dtos/signinUser.dto';
import { UserEntity } from './user.entity';
import { UsersService } from './users.service';

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UsersService) {}

  public async signup(createUserDto: CreateUserDto): Promise<UserEntity> {
    const users = await this.usersService.find(createUserDto.email);

    if (users.length) {
      throw new HttpException('Email in use!', HttpStatus.BAD_REQUEST);
      //throw new BadRequestException('Email in use');
    }

    const hashedPassword = await hashPassword(createUserDto.password);

    const user = await this.usersService.create({
      email: createUserDto.email,
      password: hashedPassword,
    });

    return user;
  }

  public async signin(signinUserDto: SignInUserDto): Promise<UserEntity> {
    const [user] = await this.usersService.find(signinUserDto.email);

    if (!user) {
      throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
    }

    const isValidPassword = await comparePassword(
      signinUserDto.password,
      user.password,
    );

    if (!isValidPassword) {
      throw new HttpException('Bad password!', HttpStatus.BAD_REQUEST);
    }

    return user;
  }
}
