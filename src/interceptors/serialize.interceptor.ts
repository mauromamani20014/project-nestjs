import {
  UseInterceptors,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';
// import { UserDto } from '@app/users/dtos/user.dto';

// Limitando el tipo de dato aceptado en el decorator
interface ClassConstructor {
  new (...args: any[]): unknown;
}

// Transformando el interceptor en un decorator
export function Serialize(dto: ClassConstructor) {
  return UseInterceptors(new SerializeInterceptor(dto));
}

export class SerializeInterceptor implements NestInterceptor {
  constructor(private readonly classConstructor: ClassConstructor) {}

  intercept(_context: ExecutionContext, next: CallHandler): Observable<any> {
    // Run something before a request is handled
    return next.handle().pipe(
      map((data: any) => {
        // Run something before the response is sent out
        return plainToClass(this.classConstructor, data, {
          // Con esto tenemos que indicar explicitamente que es lo que queremos
          // exponer (@Expose()), lo que es dificil si tenemos muchos campos.
          // Caso contrario lo seteamos en false y usamo en el dto el decorator
          // (@Exclude()) para solo excluir lo que definimos, dejando todso los
          // valores intactos
          excludeExtraneousValues: true,
        });
      }),
    );
  }
}
