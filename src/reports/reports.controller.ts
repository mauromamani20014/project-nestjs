import {
  Controller,
  Post,
  Body,
  UseGuards,
  Patch,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { AdminGuard } from '@app/guards/admin.guard';
import { AuthGuard } from '@app/guards/auth.guard';
import { User } from '@app/users/decorators/user.decorator';
import { UserEntity } from '@app/users/user.entity';
import { Serialize } from '../interceptors/serialize.interceptor';

import { CreateReportDto } from './dtos/create-report.dto';
import { SerializeReportDto } from './dtos/serialize-report.dto';
import { ApproveReportDto } from './dtos/approved-report.dto';
import { ReportEntity } from './report.entity';
import { ReportsService } from './reports.service';
import { GetEstimateDto } from './dtos/get-estimate.dto';

@Controller('/reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Post()
  @UseGuards(AuthGuard)
  @Serialize(SerializeReportDto)
  public createReport(
    @User() user: UserEntity,
    @Body('report') createReportDto: CreateReportDto,
  ): Promise<ReportEntity> {
    return this.reportsService.create(createReportDto, user);
  }

  @Patch('/:id')
  @UseGuards(AdminGuard)
  public approveReport(
    @Param('id') id: number,
    @Body('report') approveReportDto: ApproveReportDto,
  ): Promise<ReportEntity> {
    return this.reportsService.changeApproval(id, approveReportDto);
  }

  @Get()
  public async getEstimate(
    @Query() getEstimateDto: GetEstimateDto,
  ): Promise<any> {
    return this.reportsService.createEstimate(getEstimateDto);
  }
}
