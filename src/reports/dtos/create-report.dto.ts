import {
  IsNumber,
  IsString,
  Min,
  Max,
  IsLongitude,
  IsLatitude,
} from 'class-validator';

export class CreateReportDto {
  @IsNumber()
  @Min(0)
  @Max(1000000)
  readonly price: number;

  @IsString()
  readonly make: string;

  @IsString()
  readonly model: string;

  @IsNumber()
  @Min(1900)
  @Max(2500)
  readonly year: number;

  @IsLongitude()
  readonly lgn: number;

  @IsLatitude()
  readonly lat: number;

  @IsNumber()
  @Min(0)
  @Max(100000)
  readonly mileage: number;
}
