// import {UserEntity} from '@app/users/user.entity';
import { Expose, Transform } from 'class-transformer';

export class SerializeReportDto {
  @Expose()
  readonly id: number;

  @Expose()
  readonly approved: boolean;

  @Expose()
  readonly price: number;

  @Expose()
  readonly make: string;

  @Expose()
  readonly model: string;

  @Expose()
  readonly year: number;

  @Expose()
  readonly lgn: number;

  @Expose()
  readonly lat: number;

  @Expose()
  readonly mileage: number;

  /*
   * Transformando el usuario completo de la relacion {email: .., etc} a un uid, usando @Transform
   * obj: Hace referencia al objeto que se esta serializando, en este caso accedemos a la
   * propiedad user dentro del objeto report
   * */
  @Transform(({ obj }) => obj.user.id)
  @Expose()
  readonly userId: number;
}
