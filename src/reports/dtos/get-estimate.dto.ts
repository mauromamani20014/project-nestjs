import {
  IsNumber,
  IsString,
  Min,
  Max,
  IsLongitude,
  IsLatitude,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class GetEstimateDto {
  @IsString()
  readonly make: string;

  @IsString()
  readonly model: string;

  @Transform(({ value }) => parseInt(value))
  @IsNumber()
  @Min(1900)
  @Max(2500)
  readonly year: number;

  @Transform(({ value }) => parseFloat(value))
  @IsLongitude()
  readonly lgn: number;

  @Transform(({ value }) => parseFloat(value))
  @IsLatitude()
  readonly lat: number;

  @Transform(({ value }) => parseInt(value))
  @IsNumber()
  @Min(0)
  @Max(100000)
  readonly mileage: number;
}
