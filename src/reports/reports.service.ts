import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '@app/users/user.entity';

import { ReportEntity } from './report.entity';
import { CreateReportDto } from './dtos/create-report.dto';
import { ApproveReportDto } from './dtos/approved-report.dto';
import { GetEstimateDto } from './dtos/get-estimate.dto';

@Injectable()
export class ReportsService {
  constructor(
    @InjectRepository(ReportEntity)
    private readonly reportRepository: Repository<ReportEntity>,
  ) {}

  public async create(
    createReportDto: CreateReportDto,
    user: UserEntity,
  ): Promise<ReportEntity> {
    const report = await this.reportRepository.create(createReportDto);
    report.user = user;

    return this.reportRepository.save(report);
  }

  public async changeApproval(
    id: number,
    approveReportDto: ApproveReportDto,
  ): Promise<ReportEntity> {
    const report = await this.reportRepository.findOne(id);

    if (!report) {
      throw new HttpException('Report not found!', HttpStatus.NOT_FOUND);
    }

    report.approved = approveReportDto.approved;

    return this.reportRepository.save(report);
  }

  public async createEstimate(getEstimateDto: GetEstimateDto): Promise<any> {
    return (
      this.reportRepository
        .createQueryBuilder()
        .select('AVG(price)', 'price')
        .where('make = :make', { make: getEstimateDto.make })
        // Si escribimos un where se sobreescribirá, mejor usamos andWhere
        .andWhere('model = :model', { model: getEstimateDto.model })
        // Encontrar los vehiculos que esten a una distancia de 5 radio de la lgn y lat dada
        .andWhere('lgn - :lgn BETWEEN -5 AND 5', { lgn: getEstimateDto.lgn })
        .andWhere('lat - :lat BETWEEN -5 AND 5', { lat: getEstimateDto.lat })
        .andWhere('year - :year BETWEEN -3 AND 3', {
          year: getEstimateDto.year,
        })
        .andWhere('approved IS TRUE')
        .orderBy('ABS(mileage - :mileage)', 'DESC')
        .setParameters({ mileage: getEstimateDto.mileage })
        .limit(3)
        .getRawOne()
    );
  }
}
