import { NestFactory } from '@nestjs/core';
//import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
//import { setupApp } from './setup-app';
//const cookieSession = require('cookie-session');

/*
 * REQUEST -> MIDDLEWARE -> GUARD -> INTERCEPTOR -> HANDLE REQUEST -> INTERCEPTOR -> RESPONSE
 * */
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  /*
  app.use(
    cookieSession({
      keys: ['secrete-value'],
    }),
  );
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));*/
  await app.listen(3000);
}
bootstrap();
